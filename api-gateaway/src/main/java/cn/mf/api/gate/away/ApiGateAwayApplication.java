package cn.mf.api.gate.away;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author cityre
 * @date 2019/3/5
 */
@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
public class ApiGateAwayApplication {
	public static void main(String[] args) {
		SpringApplication.run(ApiGateAwayApplication.class);
	}
}
