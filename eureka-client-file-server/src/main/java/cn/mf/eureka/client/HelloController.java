package cn.mf.eureka.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author cityre
 * @date 2019/3/4
 */
@RestController
@Slf4j
public class HelloController {
	
	@PostMapping("/upload")
	public String upload(@RequestPart("file") MultipartFile file) {
		return file.getOriginalFilename();
	}
}
