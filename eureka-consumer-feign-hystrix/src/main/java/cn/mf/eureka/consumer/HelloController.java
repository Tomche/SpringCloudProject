package cn.mf.eureka.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author cityre
 * @date 2019/3/4
 */
@RestController
public class HelloController {
	private final HelloService helloService;
	
	@Autowired
	public HelloController(@Qualifier("eureka-client") HelloService helloService) {
		this.helloService = helloService;
	}
	
	
	@GetMapping("/say")
	public String say() {
		return helloService.sayHello();
	}
}
