package cn.mf.eureka.consumer;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author cityre
 * @date 2019/3/4
 */
@FeignClient(value = "eureka-client", fallback = HelloServiceCallback.class)
public interface HelloService {
	
	@GetMapping("/hello")
	String sayHello();
	
}
