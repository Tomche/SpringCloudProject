package cn.mf.eureka.consumer;

import org.springframework.stereotype.Component;

/**
 * @author cityre
 * @date 2019/3/4
 */
@Component
public class HelloServiceCallback implements HelloService {
	@Override
	public String sayHello() {
		return "fallback";
	}
}
