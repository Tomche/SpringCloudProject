package cn.mf.eureka.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author cityre
 * @date 2019/3/4
 */
@RestController
public class HelloController {
	private final HelloService helloService;
	
	@Autowired
	public HelloController(HelloService helloService) {
		this.helloService = helloService;
	}
	
	
	@PostMapping(value = "/file/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String upload(@RequestPart("file") MultipartFile file) {
		return helloService.handleFileUpload(file);
	}
}
