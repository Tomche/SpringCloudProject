package cn.mf.eureka.consumer;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author cityre
 * @date 2019/3/4
 */
@RestController
public class HelloController {
	
	@Autowired
	private  ConsumerService consumerService;
	

	
	@GetMapping("/say-ribbon")
	public String say() {
		return consumerService.consumer();
	}
	@Service
	class ConsumerService {
		
		@Autowired
		RestTemplate restTemplate;
		
		@HystrixCommand(fallbackMethod = "fallback")
		public String consumer() {
			return restTemplate.getForObject("http://eureka-client/hello", String.class);
		}
		
		public String fallback() {
			return "fallbck";
		}
		
	}
}
