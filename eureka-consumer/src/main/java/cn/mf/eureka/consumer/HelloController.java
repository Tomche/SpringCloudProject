package cn.mf.eureka.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author cityre
 * @date 2019/3/4
 */
@RestController
public class HelloController {
	
	private final RestTemplate restTemplate;
	
	
	@Autowired
	public HelloController(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@GetMapping("/say")
	public String say() {
		
		String url = "http://localhost:8002/hello";
		
		return restTemplate.getForObject(url, String.class);
	}
}
