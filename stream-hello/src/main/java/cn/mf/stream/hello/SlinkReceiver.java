package cn.mf.stream.hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

/**
 * @author cityre
 * @date 2019/3/5
 */
@EnableBinding(Sink.class)
public class SlinkReceiver {
	
	private static Logger logger = LoggerFactory.getLogger(SlinkReceiver.class);
	
	@StreamListener(Sink.INPUT)
	public void receive(Object payload) {
		logger.info("Received: " + payload);
	}
	
}
