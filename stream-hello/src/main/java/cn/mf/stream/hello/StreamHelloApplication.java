package cn.mf.stream.hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author cityre
 * @date 2019/3/5
 */
@SpringBootApplication
public class StreamHelloApplication {
	public static void main(String[] args) {
		SpringApplication.run(StreamHelloApplication.class);
	}
}
